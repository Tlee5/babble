require_relative "tile_group.rb"
require_relative "tile_bag.rb"

#represents a scorable word
class Word < TileGroup

#initializes the class
def initialize 
 @tiles = []
end

#returns the sum of all points for the tiles this word contains
def score
score = 0
 if @tiles.size > 0 
  @tiles.each{|x| score = score + TileBag.points_for(x)}
 end
score
end

end
