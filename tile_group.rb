#represents a group of tiles 
class TileGroup

#initializes the TileGroup
def initialize
@tiles =[]
end

#creates accessor for tiles
attr_accessor :tiles


#appends a tile to the group
def append(tile)
 if tile.is_a? Symbol
  @tiles.push(tile)
 else
  puts "Tile is not a symbol"
 end
end

#removes a tile from the group
def remove(tile)
 if (tile.is_a? Symbol) && (@tiles.size > 0) && (@tiles.include?(tile))
  index = @tiles.index(tile)
  @tiles.delete_at(index)
 else
  puts "Tile is not a present in group"
 end
end

#returns string that is concatenation of all tles' string values
def hand
letters = ""
 if (@tiles != nil)
  @tiles.each {|x| letters.concat("#{x}")}
 end
letters
end

end

