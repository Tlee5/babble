require_relative "tile_group.rb"
require_relative "word.rb"

#represents a tile rack
class TileRack < TileGroup

#initializes the tile rack
def initialize
 @tiles = []
end

#returns the number of tiles needed to replenish the tile rack
def number_of_tiles_needed
 if @tiles.size <= 7
  return needed = 7 - @tiles.size
 else
  puts "Invalid number of tiles on tile rack"
 end
end

#returns true if the tile rack has the needed tiles to complete the given word
def has_tiles_for?(text)
 if text.is_a? String
  result = true
  symbols = []
  temp_tiles = @tiles.dup
  text.each_char{|x| symbols.push(x.to_sym)}
  symbols.each{|x|
   if temp_tiles.include?(x) && result == true
    index = temp_tiles.index(x)
    temp_tiles.delete_at(index)
   else
    result = false
   end
  }
  return result
 else
  puts "Input is not a string!"
 end
end

#returns a word object made by removing tiles given by text
def remove_word(text)
 if text.is_a? String
  word = Word.new
  symbols = []
  text.each_char{|x| symbols.push(x.to_sym)}
  symbols.each{|x|
   if @tiles.include?(x)
    word.append(x)
    index = @tiles.index(x)
    @tiles.delete_at(index)
   else
    puts "tile rack does not contain text"
   end
  }
  return word
 else
  puts "Text is not a string!"
 end
end

end

