require_relative "word.rb"
require_relative "tile_rack.rb"
require_relative "tile_bag.rb"
require 'spellchecker'
require 'tempfile'

#represents a driver class for the Babble program
class Babble

#creates babble objects
def initialize
@bag = TileBag.new
@word = Word.new
@current_rack = TileRack.new
end

#runs the program
def run
7.times do |i|
 @current_rack.append(@bag.draw_tile)
end
score = 0
response = ""
 until response.eql? ":quit"
  puts @current_rack.hand
  puts "Guess a word"
  response = gets.chomp
  result = Spellchecker.check(response)
   if response.eql? ":quit"
    response = ":quit"
   end
   if !response.eql? ":quit"
     if result.size > 1 || result[0][:correct] == false
      puts "Not a valid word"
     end
     if result[0][:correct] == true && @current_rack.has_tiles_for?(response.upcase) == false
      puts "Not enough tiles"
     end
     if result[0][:correct] == true && @current_rack.has_tiles_for?(response.upcase) == true
      response.each_char{|x| @word.append(x.upcase.to_sym)}
      @current_rack.remove_word(response.upcase)
      puts "You made #{response.upcase} for #{@word.score} points!"
      score = score + @word.score
      refill = @current_rack.number_of_tiles_needed
      refill.times do |i|
       @current_rack.append(@bag.draw_tile)
     end
      @word.tiles.clear
   end
  puts "Current total score: #{score}"
  end
 end 
puts "Thanks for playing, total score: #{score}"
end

end 
Babble.new.run
