#represents a bag of tiles for replenishing tile rack
class TileBag

#initializes the bag with a complete set of tiles

#generators getters and setters for tiles array
attr_accessor :tiles

#constructs the tilebag
def initialize
@tiles =[	
 :A, :B, :C, :D, :E, :F, :G,
 :H, :I, :J, :K, :L, :M, :N,
 :O, :P, :Q, :R, :S, :T, :U,
 :V, :W, :X, :Y, :Z ]
@tiles.fill(:A, @tiles.size, 8)
@tiles.fill(:B, @tiles.size, 1)
@tiles.fill(:C, @tiles.size, 1)
@tiles.fill(:D, @tiles.size, 3)
@tiles.fill(:E, @tiles.size, 11)
@tiles.fill(:F, @tiles.size, 1)
@tiles.fill(:G, @tiles.size, 2)
@tiles.fill(:H, @tiles.size, 1)
@tiles.fill(:I, @tiles.size, 8)
@tiles.fill(:L, @tiles.size, 3)
@tiles.fill(:M, @tiles.size, 1)
@tiles.fill(:N, @tiles.size, 5)
@tiles.fill(:O, @tiles.size, 7)
@tiles.fill(:P, @tiles.size, 1)
@tiles.fill(:R, @tiles.size, 5)
@tiles.fill(:S, @tiles.size, 3)
@tiles.fill(:T, @tiles.size, 5)
@tiles.fill(:U, @tiles.size, 3)
@tiles.fill(:V, @tiles.size, 1)
@tiles.fill(:W, @tiles.size, 1)
@tiles.fill(:Y, @tiles.size, 1)
end

#removes one random tile from the bag and returns it if bag is not empty
def draw_tile
 if @tiles.empty? == false
  tile = @tiles.sample
  index = @tiles.index(tile)
  @tiles.delete_at(index)
  return tile
 end
"There are no more tiles!"
end

#returns true if the bag is empty, false if it is not
def empty?
@tiles.empty?
end


#returns the points for the given tile letter
def self.points_for(tile)
points = { A: 1, B: 3, C: 3, D: 2, E: 1, F: 4, G: 2, H: 4, I: 1, J: 8, K: 5, L: 1, M: 3, N: 1, O: 1, P: 3, Q: 10, R: 1, S: 1, T: 1, U: 1, V: 4, W: 4, X: 8, Y: 4, Z: 10 }
points[tile]
end

end
