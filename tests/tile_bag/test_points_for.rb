require "minitest/autorun"
require_relative "../../tile_bag.rb"
class TileBag::TestPointsFor < Minitest::Test
	
	#initializes TileBag
		def setup
		@bag = TileBag.new 
	end
	
	#checks to make sure correct point values are returned for points_for(tile) method

	def test_confirm_point_values
		one_point = [:A, :E, :I, :O, :N, :R, :T, :L, :S, :U]
		two_point = [:D, :G]
		three_point = [:B, :C, :M, :P]
		four_point = [:F, :H, :V, :W, :Y]
		five_point = [:K]
		eight_point = [:J, :X]
		ten_point = [:Q, :Z]
		one_point.each{|x| assert_equal 1, TileBag.points_for(x)}
		two_point.each{|x| assert_equal 2, TileBag.points_for(x)}
		three_point.each{|x| assert_equal 3, TileBag.points_for(x)}
		four_point.each{|x| assert_equal 4, TileBag.points_for(x)}
		five_point.each{|x| assert_equal 5, TileBag.points_for(x)}
		eight_point.each{|x| assert_equal 8, TileBag.points_for(x)}
		ten_point.each{|x| assert_equal 10, TileBag.points_for(x)}

	end
end
