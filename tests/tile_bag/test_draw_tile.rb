require "minitest/autorun"
require "../../tile_bag.rb"
class TileBag::TestDrawTile < Minitest::Test
	
		def setup
		@bag = TileBag.new 
	end

	#tests to make sure appropriate number of tiles can be drawn from tilebag
	def test_has_proper_number_of_tiles
	98.times do |i|		
		@bag.draw_tile
	end	
	assert_equal 0 , @bag.tiles.size
	assert_equal true, @bag.empty?
	assert_equal "There are no more tiles!", @bag.draw_tile
	end

	#tests to make sure tilebag has appropriate numbers of each letter
	def test_has_proper_tile_distribution
	assert_equal 9, @bag.tiles.count(:A)
	assert_equal 2, @bag.tiles.count(:B)
	assert_equal 2, @bag.tiles.count(:C)
	assert_equal 4, @bag.tiles.count(:D)
	assert_equal 12, @bag.tiles.count(:E)
	assert_equal 2, @bag.tiles.count(:F)
	assert_equal 3, @bag.tiles.count(:G)
	assert_equal 2, @bag.tiles.count(:H)
	assert_equal 9, @bag.tiles.count(:I)
	assert_equal 1, @bag.tiles.count(:J)
	assert_equal 1, @bag.tiles.count(:K)
	assert_equal 4, @bag.tiles.count(:L)
	assert_equal 2, @bag.tiles.count(:M)
	assert_equal 6, @bag.tiles.count(:N)
	assert_equal 8, @bag.tiles.count(:O)
	assert_equal 2, @bag.tiles.count(:P)
	assert_equal 1, @bag.tiles.count(:Q)
	assert_equal 6, @bag.tiles.count(:R)
	assert_equal 4, @bag.tiles.count(:S)
	assert_equal 6, @bag.tiles.count(:T)
	assert_equal 4, @bag.tiles.count(:U)
	assert_equal 2, @bag.tiles.count(:V)
	assert_equal 2, @bag.tiles.count(:W)
	assert_equal 1, @bag.tiles.count(:X)
	assert_equal 2, @bag.tiles.count(:Y)
	assert_equal 1, @bag.tiles.count(:Z)
	end
end
