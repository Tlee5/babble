require "minitest/autorun"
require_relative "../../tile_rack.rb"

#tests the number of tiles needed method
class TileRack::TestNumberOfTilesNeeded < Minitest::Test

#sets up TileRack
def setup
@tile_rack = TileRack.new
end

#tests that empty tile rack should need 7 tiles
def test_empty_tile_rack_should_need_max_tiles
assert_equal 7, @tile_rack.number_of_tiles_needed
end

#tests that tile rack with one tile should need 6 tiles
def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
@tile_rack.append(:M)
assert_equal 6, @tile_rack.number_of_tiles_needed
end

#tests that tile rack with several tiles should need some tiles
def test_tile_rack_with_several_tiles_should_need_some_tiles
@tile_rack.append(:D)
@tile_rack.append(:U)
@tile_rack.append(:D)
@tile_rack.append(:E)
assert_equal 3, @tile_rack.number_of_tiles_needed
end

#tests that full tile rack does not need any tiles
def test_that_full_tile_rack_doesnt_need_any_tiles
@tile_rack.append(:S)
@tile_rack.append(:T)
@tile_rack.append(:E)
@tile_rack.append(:L)
@tile_rack.append(:L)
@tile_rack.append(:A)
@tile_rack.append(:R)
assert_equal 0, @tile_rack.number_of_tiles_needed
end


end
