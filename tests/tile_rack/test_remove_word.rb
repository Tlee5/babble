require "minitest/autorun"
require_relative "../../tile_rack.rb"
require_relative"../../word.rb"

#tests the remove word method
class TileRack::TestRemoveWord < Minitest::Test

#sets up TileRack
def setup
@tile_rack = TileRack.new
end

#tests that a word can be removed from the tile rack with letters in order
def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
@tile_rack.append(:U)
@tile_rack.append(:F)
@tile_rack.append(:O)
@tile_rack.append(:B)
@tile_rack.append(:R)
@tile_rack.append(:T)
@tile_rack.append(:E)
word_result = @tile_rack.remove_word("UFO")
leftover = ""
@tile_rack.tiles.each{|x| leftover += x.to_s}
string_result = "" 
word_result.tiles.each{|x| string_result += x.to_s}
word = Word.new
word.append(:U)
word.append(:F)
word.append(:O)
assert_equal "BRTE", leftover
assert_equal "UFO", string_result
assert_equal word.score, word_result.score
assert_equal 3, @tile_rack.number_of_tiles_needed
end

#tests that a word can be removed from the tile rack with the letters not in order
def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
@tile_rack.append(:A)
@tile_rack.append(:N)
@tile_rack.append(:T)
@tile_rack.append(:E)
@tile_rack.append(:L)
@tile_rack.append(:S)
@tile_rack.append(:I)
word_result = @tile_rack.remove_word("ALIENS")
leftover = ""
@tile_rack.tiles.each{|x| leftover += x.to_s}
string_result = "" 
word_result.tiles.each{|x| string_result += x.to_s}
word = Word.new
word.append(:A)
word.append(:L)
word.append(:I)
word.append(:E)
word.append(:N)
word.append(:S)
assert_equal "T", leftover
assert_equal "ALIENS", string_result
assert_equal word.score, word_result.score
assert_equal 6, @tile_rack.number_of_tiles_needed
end

#tests can remove a word with duplicate letters
def test_can_remove_word_with_duplicate_letters
@tile_rack.append(:R)
@tile_rack.append(:O)
@tile_rack.append(:S)
@tile_rack.append(:W)
@tile_rack.append(:E)
@tile_rack.append(:L)
@tile_rack.append(:L)
word_result = @tile_rack.remove_word("ROSWELL")
leftover = ""
@tile_rack.tiles.each{|x| leftover += x.to_s}
string_result = "" 
word_result.tiles.each{|x| string_result += x.to_s}
word = Word.new
word.append(:R)
word.append(:O)
word.append(:S)
word.append(:W)
word.append(:E)
word.append(:L)
word.append(:L)
assert_equal "", leftover
assert_equal "ROSWELL", string_result
assert_equal word.score, word_result.score
assert_equal 7, @tile_rack.number_of_tiles_needed
end

#test can remove word without removing unneeded duplicate letters
def test_can_remove_word_without_removing_unneeded_duplicate_letters
@tile_rack.append(:P)
@tile_rack.append(:E)
@tile_rack.append(:A)
@tile_rack.append(:C)
@tile_rack.append(:E)
@tile_rack.append(:R)
@tile_rack.append(:E)
word_result = @tile_rack.remove_word("PACE")
leftover = ""
@tile_rack.tiles.each{|x| leftover += x.to_s}
string_result = "" 
word_result.tiles.each{|x| string_result += x.to_s}
word = Word.new
word.append(:P)
word.append(:A)
word.append(:C)
word.append(:E)
assert_equal "ERE", leftover
assert_equal "PACE", string_result
assert_equal word.score, word_result.score
assert_equal 4, @tile_rack.number_of_tiles_needed
end

end

