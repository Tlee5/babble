require "minitest/autorun"
require_relative "../../tile_rack.rb"

#tests the has tiles for method of the TileRack class
class TileRack::TestHasTilesFor < Minitest::Test

#sets up TileRack
def setup
@tile_rack = TileRack.new
end

#tests that tile rack has needed letters when letters are in order with no duplicates
def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
@tile_rack.append(:A)
@tile_rack.append(:L)
@tile_rack.append(:I)
@tile_rack.append(:E)
@tile_rack.append(:N)
@tile_rack.append(:S)
assert_equal true , @tile_rack.has_tiles_for?("ALIENS")
end

#tests that tile rack has needed letters when letters are not in order with no duplicates
def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
@tile_rack.append(:A)
@tile_rack.append(:L)
@tile_rack.append(:I)
@tile_rack.append(:E)
@tile_rack.append(:N)
@tile_rack.append(:S)
assert_equal true, @tile_rack.has_tiles_for?("NSIALE")
end

#tests a tile rack that does not have any needed letters
def test_rack_doesnt_contain_any_needed_letters
@tile_rack.append(:A)
@tile_rack.append(:L)
@tile_rack.append(:I)
@tile_rack.append(:E)
@tile_rack.append(:N)
@tile_rack.append(:S)
assert_equal false, @tile_rack.has_tiles_for?("POW")
end

#tests a tile rack that contains some but not all needed letters
def test_rack_contains_some_but_not_all_needed_letters
@tile_rack.append(:A)
@tile_rack.append(:L)
@tile_rack.append(:I)
@tile_rack.append(:E)
@tile_rack.append(:N)
@tile_rack.append(:S)
assert_equal false, @tile_rack.has_tiles_for?("MULDER")
end

#tests a tile rack that contains a word with duplicate letters
def test_rack_contains_a_word_with_duplicate_letters
@tile_rack.append(:P)
@tile_rack.append(:E)
@tile_rack.append(:A)
@tile_rack.append(:C)
@tile_rack.append(:E)
assert_equal true, @tile_rack.has_tiles_for?("PEACE")
end

#tests a tile rack that doesn't contain enough duplicate letters
def test_rack_doesnt_contain_enough_duplicate_letters
@tile_rack.append(:S)
@tile_rack.append(:C)
@tile_rack.append(:U)
@tile_rack.append(:L)
@tile_rack.append(:Y)
assert_equal false, @tile_rack.has_tiles_for?("SCULLY")
end


end
