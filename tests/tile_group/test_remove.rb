require "minitest/autorun"
require_relative "../../tile_group.rb"

#tests the remove method in the TileGroup class
class TileGroup::TestRemove < Minitest::Test

#sets up TileGroup
def setup
@tiles = TileGroup.new
end

#tests removing only tile from TileGroup
def test_remove_only_tile
@tiles.append(:A)
@tiles.remove(:A)
assert_equal 0, @tiles.tiles.size
end

#tests removing first tile from many tiles
def test_remove_first_tile_from_many
@tiles.append(:W)
@tiles.append(:H)
@tiles.append(:A)
@tiles.append(:T)
@tiles.append(:S)
@tiles.append(:U)
@tiles.append(:P)
@tiles.remove(:W)
assert_equal 6, @tiles.tiles.size
assert_equal "HATSUP", @tiles.hand
end

#tests removing last tile from many tiles
def test_remove_last_tile_from_many
@tiles.append(:W)
@tiles.append(:H)
@tiles.append(:A)
@tiles.append(:T)
@tiles.append(:S)
@tiles.append(:U)
@tiles.append(:P)
@tiles.remove(:P)
assert_equal 6, @tiles.tiles.size
assert_equal "WHATSU", @tiles.hand
end

#tests removing middle tile from many tiles
def test_remove_middle_tile_from_many
@tiles.append(:W)
@tiles.append(:H)
@tiles.append(:A)
@tiles.append(:T)
@tiles.append(:S)
@tiles.append(:U)
@tiles.append(:P)
@tiles.remove(:T)
assert_equal 6, @tiles.tiles.size
assert_equal "WHASUP", @tiles.hand
end

#tests removing multiple tiles from tile group
def test_remove_multiple_tiles
@tiles.append(:W)
@tiles.append(:H)
@tiles.append(:A)
@tiles.append(:T)
@tiles.append(:S)
@tiles.append(:U)
@tiles.append(:P)
@tiles.remove(:H)
@tiles.remove(:S)
@tiles.remove(:U)
@tiles.remove(:P)
assert_equal 3, @tiles.tiles.size
assert_equal "WAT", @tiles.hand
end

#tests duplicate tiles are not removed
def test_make_sure_duplicates_are_not_removed
@tiles.append(:C)
@tiles.append(:A)
@tiles.append(:T)
@tiles.append(:S)
@tiles.append(:A)
@tiles.append(:R)
@tiles.append(:E)
@tiles.append(:L)
@tiles.append(:I)
@tiles.append(:F)
@tiles.append(:E)
@tiles.remove(:A)
@tiles.remove(:E)
assert_equal 9, @tiles.tiles.size
assert_equal "CTSARLIFE", @tiles.hand
end

end


