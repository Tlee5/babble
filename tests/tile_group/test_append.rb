require "minitest/autorun"
require_relative "../../tile_group.rb"

#tests the append method of the TileGroup class
class TileGroup::TestAppend < Minitest::Test

#sets up TileGroup
def setup
@tiles = TileGroup.new
end

#tests appending one tile to TileGroup
def test_append_one_tile
@tiles.append(:A)
assert_equal true, @tiles::tiles.include?(:A)
assert_equal 1, @tiles.tiles.size
assert_equal "A", @tiles.hand
end

#tests appending multiple tiles to TileGroup
def test_append_many_tiles
@tiles.append(:R)
@tiles.append(:I)
@tiles.append(:P)
@tiles.append(:U)
@tiles.append(:S)
@tiles.append(:A)
assert_equal 6, @tiles.tiles.size
assert_equal "RIPUSA", @tiles.hand
end

#tests appending multiple tiles to TileGroup with duplicate tiles
def test_append_duplicate_tiles
@tiles.append(:B)
@tiles.append(:E)
@tiles.append(:A)
@tiles.append(:M)
@tiles.append(:M)
@tiles.append(:E)
@tiles.append(:U)
@tiles.append(:P)
assert_equal 8, @tiles.tiles.size
assert_equal "BEAMMEUP", @tiles.hand
end


end
