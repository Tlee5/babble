require "minitest/autorun"
require_relative "../../tile_group.rb"

#tests the remove method in the TileGroup class
class TileGroup::TestHand < Minitest::Test

#sets up TileGroup
def setup
@tiles = TileGroup.new
end

#tests converting an empty group to a string
def test_convert_empty_group_to_string
assert_equal "", @tiles.hand
end

#tests converting a single tile to a string
def test_convert_single_tile_group_to_string
@tiles.append(:A)
assert_equal "A", @tiles.hand
end

#tests converting multi tile group to string
def test_convert_multi_tile_group_to_string
@tiles.append(:S)
@tiles.append(:P)
@tiles.append(:A)
@tiles.append(:C)
@tiles.append(:E)
assert_equal "SPACE", @tiles.hand
end

#tests converting multi tile group to string with duplicates
def test_convert_multi_tile_group_with_duplicates_to_string
@tiles.append(:I)
@tiles.append(:L)
@tiles.append(:I)
@tiles.append(:K)
@tiles.append(:E)
@tiles.append(:F)
@tiles.append(:O)
@tiles.append(:O)
@tiles.append(:D)
assert_equal "ILIKEFOOD", @tiles.hand
end

end

