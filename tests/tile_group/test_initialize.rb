require "minitest/autorun"
require_relative "../../tile_group.rb"

#tests the initialize method in the TileGroup class
class TileGroup::TestInitialize < Minitest::Test

#sets up TileGroup
def setup
@tiles = TileGroup.new
end

#tests the initialize method with empty tile group
def test_create_empty_tile_group
assert_equal 0, @tiles.tiles.size
end

end
