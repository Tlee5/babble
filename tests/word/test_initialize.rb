require "minitest/autorun"
require_relative "../../word.rb"

#tests the Word#initialize method
class Word::TestInitialize < Minitest::Test

#sets up Word
def setup
@word = Word.new
end

#tests initializing and creating an empty word
def test_create_empty_word
assert_equal "", @word.hand
end

end
