require "minitest/autorun"
require_relative "../../word.rb"

#tests the Word#score method
class Word::TestScore < Minitest::Test

#sets up Word
def setup
@word = Word.new
end

#tests that an empty word should have 0
def test_empty_word_should_have_score_of_zero
assert_equal 0, @word.score
end

#tests a one tile word score
def test_socre_a_one_tile_word
@word.append(:A)
assert_equal 1, @word.score
end

#tests a score with multiple different tiles
def test_score_a_word_with_multiple_different_tiles
@word.append(:A)
@word.append(:L)
@word.append(:I)
@word.append(:E)
@word.append(:N)
@word.append(:S)
assert_equal 6, @word.score
end

#tests a score with multiple recurring tiles
def test_score_a_word_with_recurring_tiles
@word.append(:E)
@word.append(:N)
@word.append(:T)
@word.append(:E)
@word.append(:R)
@word.append(:P)
@word.append(:R)
@word.append(:I)
@word.append(:S)
@word.append(:E)
assert_equal 12, @word.score
end

end


